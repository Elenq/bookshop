from django.db import models
from cart.models import Cart
from django.contrib.auth import get_user_model
from reference.models import Status

# Create your models here.

class Order(models.Model):
    cart = models.ForeignKey(
        Cart,
        verbose_name="Корзина",
        on_delete=models.PROTECT)
    status = models.ForeignKey(
        "reference.Status",
        verbose_name="Статус заказа",
        on_delete=models.PROTECT)
    phone = models.CharField(
        verbose_name="Телефон",
        max_length=30)
    email = models.EmailField(
        verbose_name="Электронная почта",
        null=True,
        blank=True,)
    address = models.TextField(
        "Адрес доставки",
        null=True,
        blank=True,
        )
    comment = models.TextField(
        "Комментарий",
        null=True,
        blank=True,
        )
    created_date = models.DateTimeField(
        "Дата создания",
        auto_now=False,
        auto_now_add=True)
    def __str__(self):
        return "Заказ № {}, от {}".format(
            self.pk,
            self.created_date.strftime('%d.%m.%Y'))