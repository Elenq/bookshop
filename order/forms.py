from django import forms
from django.forms import ModelForm
from .models import Order


class CheckCartForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = [
            'cart',
            'status',
            'phone',
            'email',
            'address',
            'comment',
        ]
        widgets = {
            'cart': forms.HiddenInput(),
            'status': forms.HiddenInput(),
        }
