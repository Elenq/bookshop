from django.shortcuts import render
from .forms import CheckCartForm
from .models import Order
from django.views.generic import (CreateView, DetailView, ListView)
from django.urls import reverse_lazy

# Create your views here.

class OrderCheckoutView(CreateView):
    model = Order
    template_name = 'cart.html' 
    form_class = CheckCartForm
    def get_success_url(self):
        del self.request.session['cart_id']
        return reverse_lazy(
            'order:success', 
            kwargs={'pk': self.object.pk}
            )

class OrderCheckoutSuccess(DetailView):
    model = Order
    template_name = 'success.html'



class OrdersListAdmin(ListView):
    model = Order
    template_name = 'list-admin.html'
    paginate_by = 5
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["active"] = "order"
        context["total"] = self.object_list.count()
        return context


class OrderDetailView(DetailView):
    model = Order
    template_name = 'order/order-detail.html'
    def get_object(self, *args, **kwargs):
        obj = super().get_object(*args, **kwargs)
        obj.status = OrderStatus.objects.get(pk=1)
        obj.save()
        return obj