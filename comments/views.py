from django.shortcuts import render
from django.views.generic.base import RedirectView
from django.contrib.contenttypes.models import ContentType
from .models import Comment
from django.contrib import messages
from django.views.generic import DetailView


class AddComment(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        print(self, *args, **kwargs)
        next = self.request.POST.get('next', '/')
        ct_id = self.request.POST.get('ct_id')
        obj_id = self.request.POST.get('obj_id')
        comment = self.request.POST.get('comment')
        if ct_id and obj_id and comment:
            ct = ContentType.objects.get_for_id(ct_id)
            comment, created = Comment.objects.get_or_create(
                comment=comment,
                content_type=ct,
                object_id=obj_id,
                defaults={
                    'comment': comment,
                    'content_type': ct,
                    'object_id': obj_id,
                }
            )
#        messages.add_message(self.request, messages.INFO, 'Добавлен коммент.')
#        print(**kwargs)
        return next


class CommentView(DetailView):
    template_name = 'detail-base-ref.html'
    model = Comment
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['comments'] = Comment.objects.filter(
            comment=self.object.comment
        )
        return context