from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
# Create your models here.

class Comment(models.Model):
    comment = models.CharField(max_length = 25)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    def __str__ (self):
        return self.comment

    # {% for comment in comments %}
    # <li>{% if comment.tags %} class="{{ message.tags }}"{% endif %}>{{ message }}</li>
    # {% endfor %}




# 26.01.2019
# ссылки на модели в ContentType
# GenericKey = ct (какая таблица), object_id (ид объекта из этой таблицы)
#     content_type = models.ForeignKey(ContentType (указывает с какой таблицей работаем), on_delete=models.CASCADE)


#     pip install приложения с тегами в новые проекты

#     в форме hidden!
#     ct = input name="ct_id" value="{{ ct.pk }}"
#     object_id  = input name="obj_id" value="{{ obj_id }}"
#     {{ }} - есть в контексте - return dict, в котором есть ct.pk и obj_id
#     если в функции есть obj (self, obj), то тег ждет инфу об объекте, позиционный аргумент
#     {% tag.obj %} - взяли пк
#     у контентТаип - def get_for_model (отдает к контекст)

# post или get ->
# {tag: "jkk",
# ct: "9",
# object_id: "17"}

# если взять метод гет, то все данные будут в адресной строке (нехорошо для данной задачи)
# некст спрашивает текущий урл (гет фул пасс)
# шаблон рендерит контекст


# список тегов
# {% for tag in tang %}
# <li>...

# tags=Tag.objects.filter(
#     ct=ct,
#     object_id=object_id
# )

# retunn
# "tags":tags


# линк на тег
# DetailView
# url 'tag:view' tag-pk 
# -> 
# listTags(DetailView)
# прилетел пк 
# def get_context_data(self):
#     super
#     obj=context[object]
#     tag=obj.tag (взяли слово тега)
#     tags=Tag.objects.filter(
#         tag=tag
#     )
#     подкинуть в контекст
#     context['tags']=tags (список объектов)
#     return context

#     в шаблоне
#     {% for tag in tags %}
#     {{ get_abs_url }}
# но лучше сделать ссылки руками




# поиск
# новый апп
# глобальный урл - добавляем ссылку на вьюху, которая будет ловить запросы
# 'search', SearchView.as_view
# 'makesearch', MakeSearchView.as_view

# views.py
# class SearchView(TemplateView):
#     template_name = "search/search.html"

# class MakeSearchView(TemplateView):
#     template_name = "search/search_results.html"
#     def get_context_data(self, **kwargs):
#         super
#         q = self.request.GET.get('q')
#         print(q)
#         sr = Book.objects.filter(
#             name__icontains=q,
#             active = True,
#             publisher__name__icontains=q,

#             через Q | или
#             (Q(name__icontains=q) | Q(publisher__name__icontains=q)),
#             active = True
#         )

# if active: 
#     sr = sr.filter(active=True)



#         context['results'] = sr
#         return context
            # name__icontains=q,
            # active = True,
# Q(imprintdate__name__icontains=q))

# search.html
# форма для поиска
# <form method="GET" action="{% url "makesearch" %}">
# <input type="text" name="q">
# <button type="submit">искать</button>
# </form>


# search_results.html
# <h1>результаты</h1>
# {% for result in results %}
# {{ result }}
