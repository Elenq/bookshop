from django import template
from django.contrib.contenttypes.models import ContentType
from comments.models import Comment

register = template.Library()

@register.inclusion_tag('comment.html', takes_context=True)
def comment(context, obj, next='/'):
    ct = ContentType.objects.get_for_model(obj)
    comment = Comment.objects.filter(
        content_type=ct,
        object_id=obj.pk
    )
#    unique_together = ('content_type', 'object_id',)
    return {
        'ct': ct.pk,
        'obj_id': obj.pk,
        'comment': comment,
        'next': next       
    }
