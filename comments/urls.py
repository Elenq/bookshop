from django.urls import path
from . import views
from .views import AddComment

app_name = "comments"

urlpatterns = [
    path('add-comment/', AddComment.as_view(), name = "add-comment"),
]