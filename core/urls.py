from django.urls import path
from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from . import views
from django.contrib.auth.views import LoginView, LogoutView
# from .views import ProfileView
from core.views import UserHomeTemplateView, AboutTemplateView, DeliveryTemplateView

app_name = "core"

urlpatterns = [

    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('profile/', views.view_profile, name='view_profile'),
    path('about/', AboutTemplateView.as_view(), name='about'),
    path('delivery/', DeliveryTemplateView.as_view(), name='delivery'),
    
    ] 
