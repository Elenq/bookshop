from django import template
from cart.models import Cart
from reference.models import BookAuthor
from cart.models import ProductsInCart

register = template.Library()
@register.inclusion_tag('top_cart_icon.html', takes_context=True)
# число товаров в корзине
def top_cart_icon(context):
    cart_id = context['request'].session.get('cart_id')
    if cart_id:
        products = ProductsInCart.objects.filter(
            cart__pk=cart_id
        ).count()
    else:
        products = 0
    return {
        'products': products
    }
