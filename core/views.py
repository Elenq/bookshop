from django.shortcuts import render
from django.views.generic import (CreateView, UpdateView, DetailView, ListView, DeleteView)
# from .models import Profile
# from .forms import ProfileForm

# Create your views here.

from django.views.generic.base import TemplateView
from products.models import Book

class UserHomeTemplateView(TemplateView):
    template_name = "user-home.html"
    def get_context_data(self, **kwargs):
        context = super(UserHomeTemplateView, self).get_context_data(**kwargs)
        last_book = Book.objects.all()
        #[4:]
        context['last_book'] = last_book
        return context

class AboutTemplateView(TemplateView):
    template_name = "about.html"
    def get_context_data(self, **kwargs):
        context = super(AboutTemplateView, self).get_context_data(**kwargs)
        context['logo'] = "BOOKshop"
        return context

class DeliveryTemplateView(TemplateView):
    template_name = "delivery.html"
    def get_context_data(self, **kwargs):
        context = super(DeliveryTemplateView, self).get_context_data(**kwargs)
        return context

class CartTemplateView(TemplateView):
    template_name = "add-to-cart.html"
    def get_context_data(self, **kwargs):
        context = super(CartTemplateView, self).get_context_data(**kwargs)
        return context


# class ProfileView(DetailView):
#     model = Profile
#     form_class = ProfileForm
#     template_name = 'profile.html'
#     def get_context_data(self, *args, **kwargs):
#         context = super().get_context_data(*args, **kwargs)
#         context['descr'] = 'Кабинет пользователя'
#         return context

def view_profile(request):
    args = {'user': request.user}
    return render(request, 'profile.html', args)