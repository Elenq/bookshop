from django import forms

from .models import Book

class BookForm(forms.ModelForm):
    class Meta:
        model = Book
        fields = [
            'name', 
            'photo', 
            'cost',
            'author',
            'description',
            'series',
            'genre',
            'imprintdate',
            'pagecount',
            'cover',
            'bookformat',
            'isbn',
            'weight',
            'agelimit',
            'publisher',
            'maker',
            'bookquantity', 
            'active',
            'promotion',
            'ratio',
            'dateadd',
            'dateuppdate'
        ]
    '''def get_view_url(self):
        return "/admin/ref/bookseries/detail/{}/".format(self.pk)
    def get_update_url(self):
        return "/admin/ref/bookseries/update/{}/".format(self.pk)
    def get_delete_url(self):
        return "/admin/ref/bookseries/delete/{}/".format(self.pk)'''
'''
def clean_cost(self):
    cleaned_data = super().clean()
    cost = self.cleaned_data.get ("cost")
    # import validation error from django
    if cost != 5:
        raise ValidationError("Cost 5")'''

def clean(self):
    cleaned_data = super().clean()
    cost = self.cleaned_data.get ("cost")
    # import validation error from django
    if cost != 5:
        self.add_error('cost', '5')
        raise ValidationError("Cost 5")
    return cleaned_data

