from django.shortcuts import render
from django.views.generic import (CreateView, UpdateView, DetailView, ListView, DeleteView)
from .forms import BookForm
from .models import Book

from datetime import datetime, date, time, timedelta

class CreateBook(CreateView):
    form_class=BookForm
    template_name = 'create-update-book.html'
    success_url = '/admin/prod/book/list/'
    def get_context_data(self, *args, **kwargs):
        context = super(CreateBook, self).get_context_data(*args, **kwargs)
        context['ref_action'] = 'Добавление книги'
        context['logo'] = "BOOKshop"
        return context

class ListBook(ListView):
    template_name = 'list-base-book.html'
    model = Book
    paginate_by = 5
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['descr'] = 'Список книг'
        context['count_books'] = self.model.objects.all().count()
        context['active_books'] = self.model.objects.filter(active=True).count()
        x = datetime.now()-timedelta(days=1)
        context['add_books'] = self.model.objects.filter(dateadd__gt=x).count()
        y = datetime.now()-timedelta(days=30)
        context['add_books_month'] = self.model.objects.filter(dateadd__gt=y).count()
        context['logo'] = "BOOKshop"
        return context

class DetailBook(DetailView):
    template_name = 'detail-base-book.html'
    model = Book
    form_class = BookForm
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['descr'] = 'Книга'
        context['logo'] = "BOOKshop"
        return context

class UpdateBook(UpdateView):
    template_name = 'create-update-book.html'
    form_class = BookForm
    model = Book
    success_url = '/admin/prod/book/list/'
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['ref_action'] = 'Редактирование книги'
        context['temp'] = self.kwargs
        context['logo'] = "BOOKshop"
        return context

class DeleteBook(DeleteView):
    template_name = 'delete-base-book.html'
    model = Book
    success_url = '/admin/prod/book/list/'
    def get_context_data(self, *args, **kwargs):
        context = super(DeleteBook, self).get_context_data(*args, **kwargs)
#        context = super().get_context_data(*args, **kwargs)
        context['delete'] = 'Удалить'
        context['logo'] = "BOOKshop"
        return context

