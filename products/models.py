from django.db import models
from django.utils import timezone
from django.core.exceptions import ValidationError
import re
from django.core.validators import MinValueValidator, MaxValueValidator
import datetime
# Create your models here.

def my_validator(val):
    if val != 5:
        raise ValidationError('Price must be 5')

'''def isbn_validator(val):
    if val != 13:
        raise ValidationError('Lenght ISBN must be 13')

def isbn_validator(val): 
    if len(val) != 13 
    if val != re.match('\d{13}', val):
    pass
    else:
        raise ValidationError('Неправильный ISBN')'''

class Book(models.Model):
    name = models.CharField(
        verbose_name = "Название книги",
        max_length=50
    )
    photo = models.ImageField(
        verbose_name = "Фото обложки",
#        upload_to='static/', 
        )
    cost = models.DecimalField( 
        verbose_name = "Цена (BYN)",
        decimal_places=2, 
        max_digits=5,
        #validators=[my_validator],
    )
    author = models.ManyToManyField(
        'reference.BookAuthor', 
        verbose_name="Автор(ы)",
    )
    description = models.CharField(
        verbose_name = "Описание",
        max_length=250,
    )
    series = models.ForeignKey(
        'reference.BookSeries', 
        verbose_name="Серия",
        null=True, 
        blank=True,
        on_delete=models.CASCADE
    )
    genre = models.ManyToManyField(
        'reference.Genre', 
        verbose_name="Жанр(ы)",
    )
    imprintdate = models.PositiveIntegerField(
        verbose_name = "Год издания",
        validators=[
            MinValueValidator(1900, message="Укажите год позднее 1900."),
            MaxValueValidator(datetime.datetime.now().year, message="Указанный год не может больше текущего."),
        ],
        default=2018
    )
    pagecount = models.PositiveIntegerField(
        verbose_name = "Количество страниц",
        default=300
    )
    cover = models.CharField(
        verbose_name = "Переплет",
        max_length=10
    )
    bookformat = models.CharField(
        verbose_name = "Формат",
        max_length=20
    )
    isbn = models.CharField(
        verbose_name = "ISBN",
        max_length=20,
#        validators=[isbn_validator],
    )
    weight = models.IntegerField( 
        verbose_name = "Вес (гр)"
    )
    agelimit = models.IntegerField( 
        verbose_name = "Возрастные ограничения"
    )
    publisher = models.ForeignKey(
        'reference.Publisher', 
        verbose_name="Издательство",
        on_delete=models.CASCADE
    )
    maker = models.ForeignKey(
        'reference.Maker', 
        verbose_name="Изготовитель",
        on_delete=models.CASCADE
    )
    bookquantity = models.PositiveIntegerField(
        verbose_name = "Количество книг в наличии",
        default=0
    )
    active = models.BooleanField(
        verbose_name = "Активный (доступен для заказа)",
        default=True
    )
    promotion = models.BooleanField(
        verbose_name = "товар на акции",
    )
    ratio = models.FloatField( 
        verbose_name = "Рейтинг (1-10)"
    )
    dateadd = models.DateTimeField(
        verbose_name = "Дата внесения в каталог",
        default=timezone.now
    )
    dateuppdate = models.DateTimeField(
        verbose_name = "Дата последнего изменения карточки",
        blank=True, null=True
    )
    def __str__(self):
        return self.name