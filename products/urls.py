from django.urls import path, include
from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static

from .views import (CreateBook, UpdateBook, DetailBook, ListBook, 
DeleteBook, )
# from .apptoview import ListBooks

app_name = "products"

urlpatterns = [
    path('book/create/', CreateBook.as_view(), name = "book-create"),
    path('book/detail/<int:pk>/', DetailBook.as_view(), name = "book-detail"),
    path('book/list/', ListBook.as_view(), name = "book-list"),
    path('book/update/<int:pk>/', UpdateBook.as_view(), name = "book-update"),
    path('book/delete/<int:pk>/', DeleteBook.as_view(), name = "book-delete"),

#    path('book/listbooks/<int:pk>/', ListBooks.as_view(), name = "listbooks"),
] 

#if settings.DEBUG:
#    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
#    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

    