from django import forms
from django.forms import ModelForm
from .models import ProductsInCart


class AddToCartForm(ModelForm):
    class Meta:
        model = ProductsInCart
        fields = [
            'book', 
            'quantity'
        ]
        widgets = {
            'book': forms.HiddenInput()
        }



class CartForm(ModelForm):
    class Meta:
        model = ProductsInCart
        fields = [
            'book', 
            'quantity'
        ]
        widgets = {
            'book': forms.HiddenInput()
        }

