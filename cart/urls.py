from django.urls import path
from . import views
from .views import (AddProductToCartView, CartView, DelOrderView )


app_name = "cart"

urlpatterns = [
    path('add/<int:book>/', AddProductToCartView.as_view(), name="add"),
    path('bookcart/', CartView.as_view(), name="bookcart"),
    path('del/<int:pk>/', DelOrderView.as_view(), name="delete"),
]





    