from django.db import models
from django.contrib.auth import get_user_model
from products.models import Book

User = get_user_model()

class Cart(models.Model):
    user = models.ForeignKey(
        User,
        related_name='cart',
        null=True,
        blank=True,
        on_delete=models.CASCADE
    )
    created_date=models.DateTimeField(
        verbose_name='Создано',
        auto_now=False,
        auto_now_add=True,
    )
    update_date=models.DateTimeField(
        verbose_name='Изменено',
        auto_now=True,
        auto_now_add=False,
    )
    def __str__(self):
        return "Корзина №{} для пользователя {}".format(
            self.pk,
            self.user
        )
    @property
    def products_in_cart_count(self):
        total = 0
        for product in self.products_in_cart.all():
            total += product.quantity
        return total
    class Meta:
        verbose_name='Корзина',
        verbose_name_plural="Корзина"
        
class ProductsInCart(models.Model):
    cart = models.ForeignKey(
        Cart,
        verbose_name="Корзина",
        #для ссылки!(взять все дочерние сущности!)
        related_name='products_in_cart',
        on_delete=models.CASCADE
    )
    book = models.ForeignKey(
        Book,
        on_delete=models.CASCADE
    )
    quantity = models.IntegerField(
        "Количество",
        default=1,
    )
    # returns the string representation of an object 
    def __str__(self):
        return "Книга {name} в корзине {cart_id}".format(
            name=self.book.name,
            cart_id=self.cart.pk,
        )



# Cart, Product - Uniquetogether нельзя одинаковые 
