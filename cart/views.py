from django.shortcuts import render
from django.views.generic import (UpdateView, DetailView, DeleteView)

from .forms import (AddToCartForm, CartForm)
from order.forms import CheckCartForm
from .models import (Cart, ProductsInCart)
from products.models import Book
from reference.models import Status

class AddProductToCartView(UpdateView):
    # access to self.object, which is the object being updated    
    template_name = "add-to-cart.html"
    form_class = AddToCartForm
    model = ProductsInCart
    success_url = '/user/cart/bookcart/'
    # it fetchs the object 
    def get_object(self):
        # session dictionary keys
        cart_id = self.request.session.get('cart_id')
        # get parameters of defined object/book (from request!)
        book_id = self.kwargs.get('book')
        # created - boolean! Check: if cart with the given parameters exists - continue/update; else - create a new cart (using defaults).
        print(book_id)
        cart, cart_created = Cart.objects.get_or_create(
            pk = cart_id,
            defaults = {'user':self.request.user,}
        )
        # get book in cart with personal key = book_id
        book = Book.objects.get(pk=book_id)
        # check: looking up an obj with the given kwargs or creating a new obj
        book_in_cart, created = ProductsInCart.objects.get_or_create(
            cart = cart,
            book = book,
            defaults = {
            'cart':cart,
            'book':book,    
            'quantity':1
            },
        ) 
        if cart_created: 
            self.request.session['cart_id'] = cart.pk
        # add 1 in field quantity instead of doubling books 
        elif not created:
            book_in_cart.quantity += 1
        return book_in_cart
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['logo'] = "BOOKshop"
        return context

new_order_status = Status.objects.get(pk=1)

class CartView(DetailView):
    model = Cart
    template_name = 'cart.html'
    def get_object(self):
        cart_id = self.request.session.get('cart_id')
        cart, cart_created = Cart.objects.get_or_create(
            pk=cart_id,
            defaults={'user': self.request.user}
            )
        return cart
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        checkout_form = CheckCartForm()
        checkout_form.fields['cart'].initial = self.object
        checkout_form.fields['status'].initial = new_order_status
        context["form"] = checkout_form
        return context


class DelOrderView(DeleteView):
    model = ProductsInCart
    template_name = 'delete-in-cart.html'
    success_url = '/user/cart/bookcart/'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['delete'] = 'Удалить'
        return context    
