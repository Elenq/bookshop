from django.urls import path
from . import views
from .views import (SearchView, SearchResultsView)

app_name = "search"

urlpatterns = [
    path('search/', SearchView.as_view(), name = "search"),
    path('search-results/', SearchResultsView.as_view(), name = "search-results"),
]