from django.shortcuts import render
from django.views.generic.base import TemplateView
from products.models import Book
from django.db.models import Q

class SearchView(TemplateView):
    template_name = "search.html"

class SearchResultsView(TemplateView):
    template_name = "search-results.html"
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        q = self.request.GET.get('q')
        print(q)
        sr = Book.objects.filter(
            Q(name__icontains=q) | Q(author__name__icontains=q) | Q(imprintdate__icontains=q) | Q(ratio__icontains=q),
            active = True,
        )
        context['results'] = sr
        context['key'] = q
        return context
