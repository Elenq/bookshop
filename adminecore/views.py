from django.shortcuts import render

# Create your views here.

from django.views.generic.base import TemplateView

class StaffHomeTemplateView(TemplateView):
    template_name = "staff-home.html"
    def get_context_data(self, **kwargs):
        context = super(StaffHomeTemplateView, self).get_context_data(**kwargs)
        menuitems = {
            'name': 'Справочники',
            'items': [
                {'item': 'Авторы',
                'url': '/admin/ref/bookauthor/list/'},
                {'item': 'Серии',
                'url': '/admin/ref/bookseries/list/'},
                {'item': 'Жанры',
                'url': '/admin/ref/genre/list/'},
                {'item': 'Издательства',
                'url': '/admin/ref/publisher/list/'},
                {'item': 'Изготовитель',
                'url': '/admin/ref/maker/list/'},
                {'item': 'Статус заказа',
                'url': '/admin/ref/status/list/'}
            ]
        }
        context ['menu'] = menuitems
        context['logo'] = "BOOKshop"
        #print(type(context))
        return context

    

    
'''class DetailMakerView(DetailView):
    template_name = 'detail-base-ref.html'
    model = Maker
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['descr'] = 'Изготовитель'
        return context'''