from django.apps import AppConfig


class AdminecoreConfig(AppConfig):
    name = 'adminecore'
