from django.shortcuts import render
from django.http import HttpResponse
from django.views import View 
from django.views.generic import (CreateView, UpdateView, DetailView, ListView, DeleteView)
from .models import (BookAuthor, BookSeries, Genre, Publisher, Maker, Status)
from .forms import (AuthorForm, BookSeriesForm, GenreForm, PublisherForm, MakerForm, StatusForm)
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin

# BookAuthor
class CreateAuthorView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    template_name = 'create-update.html'
    form_class = AuthorForm
    model = BookAuthor
    success_url = '/admin/ref/bookauthor/list/'
#   app.action_name!
    permission_required = 'reference.create_author'
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['descr'] = 'Автор'
        context['ref_action'] = 'Добавление автора'
        context['back_to_list'] = reverse_lazy('reference:author-list')
        return context

class ListAuthorView(ListView):
    template_name = 'list-base-ref.html'
    model = BookAuthor
    paginate_by = 5
# https://habr.com/post/137530/ 
# login_url = ...
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['add_new'] = reverse_lazy('reference:author-create')
        context['descr'] = 'Список авторов'
        return context

class DetailAuthorView(DetailView):
    template_name = 'detail-base-ref.html'
    model = BookAuthor
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['descr'] = 'Автор'
        return context

class UpdateAuthorView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = 'create-update.html'
    form_class = AuthorForm
    model = BookAuthor
    success_url = '/admin/ref/bookauthor/list/'
    permission_required = 'reference.update_author'
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['ref_action'] = 'Редактирование автора'
        context['temp'] = self.kwargs
        return context

class DeleteAuthorView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = 'delete-base.html'
    model = BookAuthor
    success_url = '/admin/ref/bookauthor/list/'
    permission_required = 'reference.delete_author'
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['delete'] = 'Желаете удалить'
        return context


# BookSeries
class CreateSeriesView(PermissionRequiredMixin, CreateView):
    template_name = 'create-update.html'
    form_class = BookSeriesForm
    model = BookSeries
    success_url = '/admin/ref/series/list/'
    permission_required = 'reference.create_series'
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['descr'] = 'Серия'
        context['ref_action'] = 'Создание сериии'
        context['back_to_list'] = reverse_lazy('reference:series-list')
        return context

class ListSeriesView(ListView):
    template_name = 'list-base-ref.html'
    model = BookSeries
    paginate_by = 5
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['add_new'] = reverse_lazy('reference:series-create')
        context['descr'] = 'Список серий'
        return context

class DetailSeriesView(DetailView):
    template_name = 'detail-base-ref.html'
    model = BookSeries
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['descr'] = 'Серия'
        return context

class UpdateSeriesView(PermissionRequiredMixin, UpdateView):
    template_name = 'create-update.html'
    form_class = BookSeriesForm
    model = BookSeries
    success_url = '/admin/ref/series/list/'
    permission_required = 'reference.update_series'
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['ref_action'] = 'Редактирование серии'
        context['temp'] = self.kwargs
        return context

class DeleteSeriesView(PermissionRequiredMixin, DeleteView):
    template_name = 'delete-base.html'
    model = BookSeries
    success_url = '/admin/ref/series/list/'
    permission_required = 'reference.delete_series'
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['delete'] = 'Желаете удалить'
        return context


# Genre
class CreateGenreView(PermissionRequiredMixin, CreateView):
    template_name = 'create-update.html'
    form_class = GenreForm
    model = Genre
    success_url = '/admin/ref/genre/list/'
    permission_required = 'reference.create_genre'
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['descr'] = 'Жанр'
        context['back_to_list'] = reverse_lazy('reference:genre-list')
        context['ref_action'] = 'Создание жанра'
        return context

class ListGenreView(ListView):
    template_name = 'list-base-ref.html'
    model = Genre
    paginate_by = 5
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['descr'] = 'Список жанров'
        context['add_new'] = reverse_lazy('reference:genre-create')
        return context

class DetailGenreView(DetailView):
    template_name = 'detail-base-ref.html'
    model = Genre
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['descr'] = 'Жанр'
        return context

class UpdateGenreView(PermissionRequiredMixin, UpdateView):
    template_name = 'create-update.html'
    form_class = GenreForm
    model = Genre
    success_url = '/admin/ref/genre/list/'
    permission_required = 'reference.update_genre'
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['ref_action'] = 'Редактирование жанра'
        context['temp'] = self.kwargs
        return context

class DeleteGenreView(PermissionRequiredMixin, DeleteView):
    template_name = 'delete-base.html'
    model = Genre
    success_url = '/admin/ref/genre/list/'
    permission_required = 'reference.delete_genre'
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['delete'] = 'Желаете удалить'
        return context


# Publisher
class CreatePublisherView(PermissionRequiredMixin, CreateView):
    template_name = 'create-update.html'
    form_class = PublisherForm
    model = Publisher
    success_url = '/admin/ref/publisher/list/'
    permission_required = 'reference.create_publisher'
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['descr'] = 'Издательство'
        context['ref_action'] = 'Создание издательства'
        context['back_to_list'] = reverse_lazy('reference:publisher-list')
        return context

class ListPublisherView(ListView):
    template_name = 'list-base-ref.html'
    model = Publisher
    paginate_by = 5
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['descr'] = 'Список издательств'
        context['add_new'] = reverse_lazy('reference:publisher-create')
        return context

class DetailPublisherView(DetailView):
    template_name = 'detail-base-ref.html'
    model = Publisher
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['descr'] = 'Издательства'
        return context

class UpdatePublisherView(PermissionRequiredMixin, UpdateView):
    template_name = 'create-update.html'
    form_class = PublisherForm
    model = Publisher
    success_url = '/admin/ref/publisher/list/'
    permission_required = 'reference.update_publisher'
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['ref_action'] = 'Редактирование издательства'
        context['temp'] = self.kwargs
        return context

class DeletePublisherView(PermissionRequiredMixin, DeleteView):
    template_name = 'delete-base.html'
    model = Publisher
    success_url = '/admin/ref/publisher/list/'
    permission_required = 'reference.delete_publisher'
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['delete'] = 'Желаете удалить'
        return context


# Maker
class CreateMakerView(PermissionRequiredMixin, CreateView):
    template_name = 'create-update.html'
    form_class = MakerForm
    model = Maker
    success_url = '/admin/ref/maker/list/'
    permission_required = 'reference.create_maker'
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['descr'] = 'Изготовитель'
        context['ref_action'] = 'Создание изготовителя'
        context['back_to_list'] = reverse_lazy('reference:maker-list')
        return context

class ListMakerView(ListView):
    template_name = 'list-base-ref.html'
    model = Maker
    paginate_by = 5
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['descr'] = 'Список изготовителей'
        context['add_new'] = reverse_lazy('reference:maker-create')
        return context

class DetailMakerView(DetailView):
    template_name = 'detail-base-ref.html'
    model = Maker
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['descr'] = 'Изготовитель'
        return context

class UpdateMakerView(PermissionRequiredMixin, UpdateView):
    template_name = 'create-update.html'
    form_class = MakerForm
    model = Maker
    success_url = '/admin/ref/maker/list/'
    permission_required = 'reference.update_maker'
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['ref_action'] = 'Редактирование изготовителя'
        context['temp'] = self.kwargs
        return context

class DeleteMakerView(PermissionRequiredMixin, DeleteView):
    template_name = 'delete-base.html'
    model = Maker
    success_url = '/admin/ref/maker/list/'
    permission_required = 'reference.delete_maker'
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['delete'] = 'Желаете удалить'
        return context

# Status
class CreateStatusView(PermissionRequiredMixin, CreateView):
    template_name = 'create-update.html'
    form_class = StatusForm
    model = Status
    success_url = reverse_lazy('reference:status-list')
    permission_required = 'reference.create_status'
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['descr'] = 'Изготовитель'
        context['ref_action'] = 'Создание статуса заказа'
        context['back_to_list'] = reverse_lazy('reference:status-list')
        return context

class ListStatusView(LoginRequiredMixin, ListView):
    template_name = 'list-base-ref.html'
    model = Status
    paginate_by = 5
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['descr'] = 'Список статусов заказа'
        context['add_new'] = reverse_lazy('reference:status-create')
        return context

class DetailStatusView(LoginRequiredMixin, DetailView):
    template_name = 'detail-base-ref.html'
    model = Status
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['descr'] = 'Статус заказа'
        return context

class UpdateStatusView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = 'create-update.html'
    form_class = StatusForm
    model = Status
    success_url = '/admin/ref/status/list/'
    permission_required = 'reference.update_status'
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['ref_action'] = 'Редактирование статуса заказа'
        context['temp'] = self.kwargs
        return context

class DeleteStatusView(PermissionRequiredMixin, DeleteView):
    template_name = 'delete-base.html'
    model = Status
    success_url = '/admin/ref/status/list/'
    permission_required = 'reference.delete_status'
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['delete'] = 'Желаете удалить'
        return context


class Index(View):
    def get(self, request):
        a = request.GET
        if 'num' in a:
            return HttpResponse('HI! Im a new blog without theme and content, but {}'.format(a["num"]))
        else: 
            return HttpResponse('HI!')   


'''

class CreateAuthorView(CreateView):
    template_name = 'author-create-update.html'
    form_class = AuthorForm
    success_url = '/shop-admin/ref/author/create/' # куда перебросить после успешной операции


class UpdateAuthorView(UpdateView):
    # UpdateView отличается от CreateView тем, что ему нужно знать какую именно
    # запись в базе данных необходимо редактировать.
    # Для того чтобы показать ему кукую запись вытащить для редактирования мы
    # из path() передаем ему пару {'key_field_name': Value}
    # путем /some_path/<int: field_name>/ в path
    # 
    # 
    # Наш path: path('shop-admin/ref/author/update/<int:pk>/', UpdateAuthorView.as_view()),
    # Наш url: '/shop-admin/ref/author/update/1/'
    # 
    # в этом случае в наш UpdateAuthorView прилетит словарь {'pk': 1}
    # Что будет означать:
    # Если запрос GET - просто зашел:
    #       - из модели Author достань мне для редактирования запись у которой поле pk = 1
    #       - заполни данными из этой таблицы форму AuthorForm
    #       - положи эту форму с словарь context['form']
    #       - возьми файл шаблона 'author-create-update.html'
    #       - отдай шаблонизатору файл шаблона и контекст(с заполненной формой) для прорисовки страницы
    #       - шаблонизатор возвращает ответ с html
    # Если запрос POST - пользователь отправил форму с измененными данными:
    #       - из модели Author достань мне для редактирования запись у которой поле pk = 1
    #       - запусти проверку данных формы (валидацию)
    #       - если данные валидны:
    #           - обновить запись в базе данных у которой поле pk = 1 валидными данными из формы
    #           - перенаправить пользователя по ссылке из success_url
    #       - если данные НЕвалидны:
    #       - вернуть на отображение текущую форму с текущими(неправильными) данными пользователю назад для исправления
    #       - при этом в форме появятся поля ошибок, которые пользователь будет видеть и которые укажут что и где он ввел неправильно
    # #########################################################################
    # При функциональном подходе все эти шаги надо делать руками
    # При ClassBase - Django делает все сама...


'''





# generic relations
# app tags 
# content type модель(таблица со ссылками на модели созданные и автоматические), object_id
# привязка не к конкретному объекту, а к любому
# нужно зарегить в админке
# content_object (создает джанго)

# лучше добавить в шаблоны возможность добавлять сущности
# для этого создадим кастомный тег
# создаем папку templatetags, __init__.py, теги, шаблон

# тег показывает форму добавления
# три скрытых поля. туда поставляется из контектста
# некст - куда вернуть
# content type - импорт, объекты и методы (берется методом гет_фо_модел - запись № из бд по объект)
# object_id 

# redirectView
# 1. уникальное сочетание обеспечить неповторение тега (уникальный ключ - комбинация из трех)
# 2. get_or_create
# сообщение если создано и обратно

# message framework
# 1 класс - миксин, срабатывает после валидации ( а в редиректвью валидации нет, поэтом миксин не сработает)
# 2 def - месседж запуливается в реквест, шаблон понимает из реквеста (в контекст). 
# INFO уровень 
# текст
# экстра - подкрашивает (классы бутстрапа - в шаблоне)
# как только мессадж зарегистрирован он появляется в реквесте. по ним можно пройти циклом в шаблоне и отрисовать. 
# список тегов получется фильтром
# берется объект, берется его поле тэг и поиск 



# список комментариев и текстовое поле под детэил вью          