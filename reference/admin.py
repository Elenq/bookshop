from django.contrib import admin

# Register your models here.
from .models import BookAuthor, BookSeries, Publisher, Genre, Maker, Status
from comments.models import Comment

admin.site.register(BookAuthor)
admin.site.register(BookSeries)
admin.site.register(Genre)
admin.site.register(Publisher)
admin.site.register(Maker)
admin.site.register(Status)
admin.site.register(Comment)