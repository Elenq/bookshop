from django.db import models
from django.urls import reverse_lazy
# Create your models here.


class BookAuthor(models.Model):
    name = models.CharField(
        verbose_name = "Автор книги",
        max_length=50
    )
    decsription = models.CharField(
        verbose_name = "Описание",
        max_length=200,
        blank=True,
        null=True  
    )
    def get_view_url(self):
        return "/admin/ref/bookauthor/detail/{}/".format(self.pk)
    def get_update_url(self):
        return "/admin/ref/bookauthor/update/{}/".format(self.pk)
    def get_delete_url(self):
        return "/admin/ref/bookauthor/delete/{}/".format(self.pk)
    def get_list_url(self):
        return "/admin/ref/bookauthor/list/"
    def __str__(self):
        return self.name + ' ' + (self.decsription or '')

class BookSeries(models.Model):
    name = models.CharField(
        verbose_name = "Серия",
        max_length=50
    )
    decsription = models.CharField(
        verbose_name = "Описание",
        max_length=200,
        blank=True,
        null=True  
    )
    def get_view_url(self):
        return "/admin/ref/bookseries/detail/{}/".format(self.pk)
    def get_update_url(self):
        return "/admin/ref/bookseries/update/{}/".format(self.pk)
    def get_delete_url(self):
        return "/admin/ref/bookseries/delete/{}/".format(self.pk)
    def get_list_url(self):
        return "/admin/ref/bookseries/list/"
    def __str__(self):
        return self.name + ' ' + (self.decsription or '')

class Genre(models.Model):
    name = models.CharField(
        verbose_name = "Жанр",
        max_length=50
    )
    decsription = models.CharField(
        verbose_name = "Описание",
        max_length=200,
        blank=True,
        null=True  
    )
    def get_view_url(self):
        return "/admin/ref/genre/detail/{}/".format(self.pk)
    def get_update_url(self):
        return "/admin/ref/genre/update/{}/".format(self.pk)
    def get_delete_url(self):
        return "/admin/ref/genre/delete/{}/".format(self.pk)
    def get_list_url(self):
        return "/admin/ref/genre/list/"
    def __str__(self):
        return self.name + ' ' + (self.decsription or '')


class Publisher(models.Model):
    name = models.CharField(
        verbose_name = "Издательство",
        max_length=50
    )
    decsription = models.CharField(
        verbose_name = "Описание",
        max_length=200,
        blank=True,
        null=True  
    )
    def get_view_url(self):
        return "/admin/ref/publisher/detail/{}/".format(self.pk)
    def get_update_url(self):
        return "/admin/ref/publisher/update/{}/".format(self.pk)
    def get_delete_url(self):
        return "/admin/ref/publisher/delete/{}/".format(self.pk)
    def get_list_url(self):
        return "/admin/ref/publisher/list/"
    def __str__(self):
        return self.name + ' ' + (self.decsription or '')


class Maker(models.Model):
    name = models.CharField(
        verbose_name = "Изготовитель",
        max_length=50
    )
    decsription = models.CharField(
        verbose_name = "Описание",
        max_length=200,
        blank=True,
        null=True  
    )
    def get_view_url(self):
        return "/admin/ref/maker/detail/{}/".format(self.pk)
    def get_update_url(self):
        return "/admin/ref/maker/update/{}/".format(self.pk)
    def get_delete_url(self):
        return "/admin/ref/maker/delete/{}/".format(self.pk)
    def get_list_url(self):
        return "/admin/ref/maker/list/"
    def __str__(self):
        return self.name + ' ' + (self.decsription or '')



class Status(models.Model):
    name = models.CharField(
        verbose_name = "Статус заказа",
        max_length=20)
    decsription = models.CharField(
        verbose_name = "Описание",
        max_length=200,
        blank=True,
        null=True)
    def get_view_url(self):
        return "/admin/ref/status/detail/{}/".format(self.pk)
    def get_update_url(self):
        return "/admin/ref/status/update/{}/".format(self.pk)
    def get_delete_url(self):
        return "/admin/ref/status/delete/{}/".format(self.pk)
    def get_list_url(self):
        return "/admin/ref/status/list/"
    # returns the string representation of an object 
    def __str__(self):
        return self.name 