
from django.urls import path, include
from django.conf.urls import url
from .views import (
    CreateAuthorView, UpdateAuthorView, DetailAuthorView, ListAuthorView, DeleteAuthorView, 
    CreatePublisherView, DetailPublisherView, ListPublisherView, UpdatePublisherView, DeletePublisherView,
    CreateSeriesView, DetailSeriesView, ListSeriesView, UpdateSeriesView, DeleteSeriesView, 
    CreateGenreView, DetailGenreView, ListGenreView, UpdateGenreView, DeleteGenreView,
    CreateMakerView, DetailMakerView, ListMakerView, UpdateMakerView, DeleteMakerView, 
    CreateStatusView, DetailStatusView, ListStatusView, UpdateStatusView, DeleteStatusView,)

app_name = "reference"

urlpatterns = [
    
    path('bookauthor/create/', CreateAuthorView.as_view(), name = "author-create"),
    path('bookauthor/detail/<int:pk>/', DetailAuthorView.as_view(), name = "author-detail"),
    path('bookauthor/list/', ListAuthorView.as_view(), name = "author-list"),
    path('bookauthor/update/<int:pk>/', UpdateAuthorView.as_view(), name = "author-update"),
    path('bookauthor/delete/<int:pk>/', DeleteAuthorView.as_view(), name = "author-delete"),

    path('bookseries/create/', CreateSeriesView.as_view(), name = "series-create"),
    path('bookseries/detail/<int:pk>/', DetailSeriesView.as_view(), name = "series-detail"),
    path('bookseries/list/', ListSeriesView.as_view(), name = "series-list"),
    path('bookseries/update/<int:pk>/', UpdateSeriesView.as_view(), name = "series-update"),
    path('bookseries/delete/<int:pk>/', DeleteSeriesView.as_view(), name = "series-delete"),
#      path('book/listbooks/<int:pk>/', ListBooks.as_view(), name = "listbooks"),



    path('genre/create/', CreateGenreView.as_view(), name = "genre-create"),
    path('genre/detail/<int:pk>/', DetailGenreView.as_view(), name = "genre-detail"),
    path('genre/list/', ListGenreView.as_view(), name = "genre-list"),
    path('genre/update/<int:pk>/', UpdateGenreView.as_view(), name = "genre-update"),
    path('genre/delete/<int:pk>/', DeleteGenreView.as_view(), name = "genre-delete"),

    path('publisher/create/', CreatePublisherView.as_view(), name = "publisher-create"),
    path('publisher/detail/<int:pk>/', DetailPublisherView.as_view(), name = "publisher-detail"),
    path('publisher/list/', ListPublisherView.as_view(), name = "publisher-list"),
    path('publisher/update/<int:pk>/', UpdatePublisherView.as_view(), name = "publisher-update"),
    path('publisher/delete/<int:pk>/', DeletePublisherView.as_view(), name = "publisher-delete"),
    
    path('maker/create/', CreateMakerView.as_view(), name = "maker-create"),
    path('maker/detail/<int:pk>/', DetailMakerView.as_view(), name = "maker-detail"),
    path('maker/list/', ListMakerView.as_view(), name = "maker-list"),
    path('maker/update/<int:pk>/', UpdateMakerView.as_view(), name = "maker-update"),
    path('maker/delete/<int:pk>/', DeleteMakerView.as_view(), name = "maker-delete"),

    path('status/create/', CreateStatusView.as_view(), name = "status-create"),
    path('status/detail/<int:pk>/', DetailStatusView.as_view(), name = "status-detail"),
    path('status/list/', ListStatusView.as_view(), name = "status-list"),
    path('status/update/<int:pk>/', UpdateStatusView.as_view(), name = "status-update"),
    path('status/delete/<int:pk>/', DeleteStatusView.as_view(), name = "status-delete"),

    ]
    