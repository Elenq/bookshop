from django import forms

from .models import BookAuthor, BookSeries, Genre, Publisher, Maker, Status

class AuthorForm(forms.ModelForm):
    class Meta:
        model = BookAuthor
        fields = [
            'name', 
            'decsription', 
        ]

class BookSeriesForm(forms.ModelForm):
    class Meta:
        model = BookSeries
        fields = [
            'name',
            'decsription', 
        ]

class GenreForm(forms.ModelForm):
    class Meta:
        model = Genre
        fields = [
            'name',
            'decsription', 
        ]

class PublisherForm(forms.ModelForm):
    class Meta:
        model = Publisher
        fields = [
            'name',
            'decsription', 
        ]


class MakerForm(forms.ModelForm):
    class Meta:
        model = Maker
        fields = [
            'name',
            'decsription', 
        ]

class StatusForm(forms.ModelForm):
    class Meta:
        model = Genre
        fields = [
            'name',
            'decsription', 
        ]